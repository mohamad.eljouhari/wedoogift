package com.wdg.services;

import com.wdg.config.GiftsServiceTestConfig;
import com.wdg.enums.WalletType;
import com.wdg.exceptions.custom.DuplicatedElementException;
import com.wdg.models.JsonData;
import com.wdg.services.impl.GiftsService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("test")
@ContextConfiguration(classes = {GiftsServiceTestConfig.class})
@SpringBootTest(classes = GiftsService.class)
class GiftsServiceTest {

    @Autowired
    GiftsService service;

    @Autowired
    ICompanyService companyService;

    @Autowired
    IUserService userService;

    @Test
    void distribute() throws DuplicatedElementException {
        JsonData data = service.getData();
        int companyBalance = data.getCompanies().get(0).getBalance();
        int distributionSize = data.getDistributions().size();

        Mockito.when(companyService.findCompanyById(data.getCompanies(), 1)).thenReturn(data.getCompanies().get(0));
        Mockito.when(userService.findUserById(data.getUsers(), 1)).thenReturn(data.getUsers().get(0));
        Mockito.when(companyService.hasSufficiantBalance(data.getCompanies().get(0), 10)).thenReturn(true);

        data = service.distribute(WalletType.GIFT, 1, 1, 10);

        assertThat(data.getCompanies().get(0).getBalance()).isEqualTo(companyBalance - 10);
        assertThat(data.getDistributions()).hasSize(distributionSize + 1);
        assertThat(data.getDistributions().get(distributionSize).getAmount()).isEqualTo(10);

        companyBalance -= 10;
        distributionSize++;

        Mockito.when(companyService.hasSufficiantBalance(data.getCompanies().get(0), 20)).thenReturn(true);

        data = service.distribute(WalletType.FOOD, 1, 1, 20);

        assertThat(data.getCompanies().get(0).getBalance()).isEqualTo(companyBalance - 20);
        assertThat(data.getDistributions()).hasSize(distributionSize + 1);
        assertThat(data.getDistributions().get(distributionSize).getAmount()).isEqualTo(20);
    }

    @Test
    void calculateUserBalance() {
    }
}