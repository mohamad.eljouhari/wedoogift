package com.wdg.exceptions.custom;

public class DuplicatedElementException extends Exception {
    public DuplicatedElementException(String elementType) {
        super(String.format("Duplicated %s in source!", elementType));
    }
}
