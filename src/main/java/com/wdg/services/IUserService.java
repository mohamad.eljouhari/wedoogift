package com.wdg.services;

import com.wdg.exceptions.custom.DuplicatedElementException;
import com.wdg.exceptions.custom.UserNotFoundException;
import com.wdg.models.Balance;
import com.wdg.models.Distribution;
import com.wdg.models.User;
import com.wdg.models.Wallet;

import java.util.List;

public interface IUserService {

    User findUserById(List<User> users, long id) throws UserNotFoundException, DuplicatedElementException;

    List<Distribution> getUserDistributions(List<Distribution> distributions, long id);

    Balance calculateBalance(List<Distribution> distributions, Wallet wallet);

    boolean isStillValidBalance(Distribution distribution);

    void updateBalance(User user, long wallet, int amount);
}
