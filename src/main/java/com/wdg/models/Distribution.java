package com.wdg.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor
public class Distribution {
    private long id;
    private long wallet_id;
    private int amount;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate start_date;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate end_date;
    private long company_id;
    private long user_id;

    public Distribution id(long id) {
        this.id = id;
        return this;
    }

    public Distribution wallet_id(long wallet_id) {
        this.wallet_id = wallet_id;
        return this;
    }

    public Distribution amount(int amount) {
        this.amount = amount;
        return this;
    }

    public Distribution start_date(LocalDate start_date) {
        this.start_date = start_date;
        return this;
    }

    public Distribution end_date(LocalDate end_date) {
        this.end_date = end_date;
        return this;
    }

    public Distribution company_id(long company_id) {
        this.company_id = company_id;
        return this;
    }

    public Distribution user_id(long user_id) {
        this.user_id = user_id;
        return this;
    }
}
