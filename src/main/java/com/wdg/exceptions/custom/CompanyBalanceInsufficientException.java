package com.wdg.exceptions.custom;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class CompanyBalanceInsufficientException extends RuntimeException {
    public CompanyBalanceInsufficientException() {
        super("Insufficient balance for the asked distribution!");
    }
}
