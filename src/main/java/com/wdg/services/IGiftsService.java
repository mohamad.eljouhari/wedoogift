package com.wdg.services;

import com.wdg.enums.WalletType;
import com.wdg.exceptions.custom.CompanyNotFoundException;
import com.wdg.exceptions.custom.DuplicatedElementException;
import com.wdg.exceptions.custom.UserNotFoundException;
import com.wdg.models.Balance;
import com.wdg.models.JsonData;

import java.util.List;

public interface IGiftsService {

    JsonData distribute(WalletType distributionType, long companyId, long userId, int amount) throws CompanyNotFoundException,
            UserNotFoundException, DuplicatedElementException;

    List<Balance> calculateUserBalance(long userId) throws UserNotFoundException, DuplicatedElementException;
}
