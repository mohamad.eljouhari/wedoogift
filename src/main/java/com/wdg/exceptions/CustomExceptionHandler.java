package com.wdg.exceptions;

import com.wdg.exceptions.custom.CompanyBalanceInsufficientException;
import com.wdg.exceptions.custom.CompanyNotFoundException;
import com.wdg.exceptions.custom.UserNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());
        com.wdg.exceptions.ErrorResponse error = new com.wdg.exceptions.ErrorResponse("Server Error", details);
        return new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(com.wdg.exceptions.custom.CompanyNotFoundException.class)
    public final ResponseEntity<Object> handleCompanyNotFoundException(CompanyNotFoundException e, WebRequest request) {
        List<String> details = new ArrayList<>();
        details.add(e.getLocalizedMessage());
        com.wdg.exceptions.ErrorResponse error = new com.wdg.exceptions.ErrorResponse("Company Not Found", details);
        return new ResponseEntity(error, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(com.wdg.exceptions.custom.CompanyBalanceInsufficientException.class)
    public final ResponseEntity<Object> handleCompanyBalanceInsufficientException(CompanyBalanceInsufficientException e, WebRequest request) {
        List<String> details = new ArrayList<>();
        details.add(e.getLocalizedMessage());
        com.wdg.exceptions.ErrorResponse error = new com.wdg.exceptions.ErrorResponse("Insufficient balance", details);
        return new ResponseEntity(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(com.wdg.exceptions.custom.UserNotFoundException.class)
    public final ResponseEntity<Object> handleUserNotFoundException(UserNotFoundException e, WebRequest request) {
        List<String> details = new ArrayList<>();
        details.add(e.getLocalizedMessage());
        com.wdg.exceptions.ErrorResponse error = new ErrorResponse("User Not Found", details);
        return new ResponseEntity(error, HttpStatus.NOT_FOUND);
    }
}