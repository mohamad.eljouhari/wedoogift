package com.wdg.helpers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.wdg.models.JsonData;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Slf4j
public class FileHelper {

    public static JsonData readFile(String filePath) throws FileNotFoundException {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(new File(filePath), JsonData.class);
        } catch (IOException e) {
            log.warn(String.format("Error reading file : %s\n%s", filePath, e.getMessage()));
            throw new FileNotFoundException();
        }
    }

    public static void updateOutput(String path, JsonData body) {
        deleteFile(path);
        createFormattedJsonFile(path, body);
    }

    private static void createFormattedJsonFile(String path, JsonData body) {

        ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);

        try {
            Files.write(Path.of(path), mapper.writeValueAsString(body).getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            log.info("Unable to create/write to file : " + path);
        }
    }

    private static void deleteFile(String path) {
        try {
            Files.deleteIfExists(Paths.get(path));
        } catch (IOException e) {
            log.info("Unable to delete file at : " + path);
        }
    }
}