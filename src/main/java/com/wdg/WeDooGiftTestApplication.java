package com.wdg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WeDooGiftTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(WeDooGiftTestApplication.class, args);
    }

}
