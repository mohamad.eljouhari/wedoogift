package com.wdg.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.wdg.exceptions.custom.CompanyBalanceInsufficientException;
import com.wdg.exceptions.custom.CompanyNotFoundException;
import com.wdg.exceptions.custom.DuplicatedElementException;
import com.wdg.exceptions.custom.UserNotFoundException;
import com.wdg.models.*;
import com.wdg.services.IGiftsService;
import com.wdg.services.impl.LdapMockUserService;
import com.wdg.utils.JWTUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController("/")
@RequiredArgsConstructor
public class WeDooAPI {

    private final IGiftsService service;
    private final AuthenticationManager authenticationManager;
    private final LdapMockUserService userDetailService;
    private final JWTUtils jwtUtils;

    @PostMapping("authenticate")
    public ResponseEntity<AuthenticationResponse> createAuthenticationToken(@RequestBody AuthenticationRequest request) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
        } catch (BadCredentialsException e) {
            throw new BadCredentialsException("Incorrect username or password !", e);
        }
        final UserDetails userDetails = userDetailService.loadUserByUsername(request.getUsername());
        final String jwt = jwtUtils.generateToken(userDetails.getUsername());
        return ResponseEntity.ok(new AuthenticationResponse(jwt));
    }

    @PostMapping("distribute")
    public ResponseEntity<String> distribute(@RequestBody DistributionRequest request) throws CompanyNotFoundException,
            UserNotFoundException, CompanyBalanceInsufficientException, JsonProcessingException, DuplicatedElementException {

        ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
        JsonData data = service.distribute(request.getWalletType(), request.getCompanyId(), request.getUserId(), request.getAmount());
        return new ResponseEntity<>(mapper.writeValueAsString(data), HttpStatus.CREATED);
    }

    @GetMapping("user/{id}/balance")
    public ResponseEntity<List<Balance>> calculateUserBalance(@PathVariable("id") Long id) throws UserNotFoundException, DuplicatedElementException {
        List<Balance> balance = service.calculateUserBalance(id);
        return ResponseEntity.ok(balance);
    }
}
