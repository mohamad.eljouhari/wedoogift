package com.wdg.services;

import com.wdg.exceptions.custom.DuplicatedElementException;
import com.wdg.exceptions.custom.UserNotFoundException;
import com.wdg.models.Distribution;
import com.wdg.models.User;
import com.wdg.models.Wallet;
import com.wdg.services.impl.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;


@SpringBootTest(classes = UserService.class)
class UserServiceTest {

    @Autowired
    UserService userService;

    @Test
    void findUserById() throws DuplicatedElementException {
        List<User> users = new ArrayList<>();
        User user1 = new User();
        user1.setId(1);
        User user2 = new User();
        user2.setId(2);
        users.add(user1);
        users.add(user2);

        assertThat(userService.findUserById(users, 1)).isNotNull();
        assertThat(userService.findUserById(users, 1).getId()).isEqualTo(1);

        assertThatExceptionOfType(UserNotFoundException.class)
                .isThrownBy(() -> userService.findUserById(users, 3));

        User user = new User();
        user.setId(1);
        users.add(user);

        assertThatExceptionOfType(DuplicatedElementException.class)
                .isThrownBy(() -> userService.findUserById(users, 1));
    }

    @Test
    void calculateBalance() {
        Distribution distribution1 = new Distribution()
                .wallet_id(1)
                .start_date(LocalDate.now().minusDays(1))
                .end_date(LocalDate.now().plusDays(1))
                .amount(10);
        Distribution distribution2 = new Distribution()
                .wallet_id(2)
                .start_date(LocalDate.now().minusDays(1))
                .end_date(LocalDate.now().plusDays(1))
                .amount(10);
        Distribution distribution3 = new Distribution()
                .wallet_id(1)
                .start_date(LocalDate.now().minusDays(3))
                .end_date(LocalDate.now().minusDays(1))
                .amount(10);
        List<Distribution> distributions = List.of(distribution1, distribution2, distribution3);

        Wallet wallet = new Wallet();
        wallet.setId(1);
        assertThat(userService.calculateBalance(distributions, wallet).getAmount()).isEqualTo(10);
    }

    @Test
    void updateBalance() {
        User user = new User();
        user.setBalance(new ArrayList<>());

        assertThat(user.getBalance()).hasSize(0);

        userService.updateBalance(user, 1, 10);

        assertThat(user.getBalance()).hasSize(1);
        assertThat(user.getBalance().get(0).getAmount()).isEqualTo(10);

        userService.updateBalance(user, 1, 10);
        assertThat(user.getBalance().get(0).getAmount()).isEqualTo(20);

        userService.updateBalance(user, 2, 10);
        assertThat(user.getBalance().get(0).getAmount()).isEqualTo(20);
        assertThat(user.getBalance().get(1).getAmount()).isEqualTo(10);
    }

    @Test
    void isStillValidBalance() {
        Distribution distribution1 = new Distribution()
                .start_date(LocalDate.now().minusDays(1))
                .end_date(LocalDate.now().plusDays(1));
        Distribution distribution2 = new Distribution()
                .start_date(LocalDate.now().minusDays(2))
                .end_date(LocalDate.now().minusDays(1));
        Distribution distribution3 = new Distribution()
                .start_date(LocalDate.now())
                .end_date(LocalDate.now());

        assertThat(userService.isStillValidBalance(distribution1)).isTrue();
        assertThat(userService.isStillValidBalance(distribution3)).isTrue();
        assertThat(userService.isStillValidBalance(distribution2)).isFalse();
    }


}