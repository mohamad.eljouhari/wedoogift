package com.wdg.exceptions.custom;

import java.io.IOException;

public class NoInputFileException extends Exception {
    public NoInputFileException(IOException i) {
        super("No input file found !", i);
    }
}
