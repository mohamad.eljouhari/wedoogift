package com.wdg.services.impl;

import com.wdg.enums.Element;
import com.wdg.exceptions.custom.DuplicatedElementException;
import com.wdg.exceptions.custom.UserNotFoundException;
import com.wdg.models.Balance;
import com.wdg.models.Distribution;
import com.wdg.models.User;
import com.wdg.models.Wallet;
import com.wdg.services.IUserService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class UserService implements IUserService {

    @Override
    public User findUserById(List<User> users, long id) throws UserNotFoundException, DuplicatedElementException {
        List<User> matches = users.stream().filter(u -> u.getId() == id).collect(Collectors.toList());
        if (matches.isEmpty())
            throw new UserNotFoundException(String.valueOf(id));
        else if (matches.size() > 1)
            throw new DuplicatedElementException(Element.USER.label);
        else
            return matches.get(0);
    }

    @Override
    public List<Distribution> getUserDistributions(List<Distribution> distributions, long userId) {
        return distributions.stream().filter(d -> d.getUser_id() == userId).collect(Collectors.toList());
    }

    @Override
    public Balance calculateBalance(List<Distribution> distributions, Wallet wallet) {
        return new Balance(wallet.getId(),
                distributions.stream()
                        .filter(d -> d.getWallet_id() == wallet.getId())
                        .filter(this::isStillValidBalance)
                        .mapToInt(Distribution::getAmount)
                        .sum()
        );
    }

    @Override
    public boolean isStillValidBalance(Distribution distribution) {
        LocalDate today = LocalDate.now();
        return distribution.getStart_date().compareTo(today) <= 0
                && distribution.getEnd_date().compareTo(today) >= 0;
    }

    @Override
    public void updateBalance(User user, long wallet, int amount) {
        Balance balance = null;
        for (Balance b : user.getBalance()) {
            if (b.getWallet_id() == wallet) {
                b.setAmount(b.getAmount() + amount);
                balance = b;
            }
        }

        if (Objects.isNull(balance))
            user.getBalance().add(new Balance(wallet, amount));
    }
}
