package com.wdg.services;

import com.wdg.exceptions.custom.CompanyBalanceInsufficientException;
import com.wdg.exceptions.custom.CompanyNotFoundException;
import com.wdg.exceptions.custom.DuplicatedElementException;
import com.wdg.models.Company;
import com.wdg.services.impl.CompanyService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

@SpringBootTest(classes = CompanyService.class)
class CompanyServiceTest {

    @Autowired
    CompanyService service;

    @Test
    void findCompanyById() throws DuplicatedElementException {
        List<Company> companies = new ArrayList<>();
        Company company1 = new Company();
        company1.setId(1);
        Company company2 = new Company();
        company2.setId(2);
        companies.add(company1);
        companies.add(company2);

        assertThat(service.findCompanyById(companies, 1)).isNotNull();
        assertThat(service.findCompanyById(companies, 1).getId()).isEqualTo(1);

        assertThatExceptionOfType(CompanyNotFoundException.class).isThrownBy(() -> {
            service.findCompanyById(companies, 3);
        });

        Company company3 = new Company();
        company3.setId(1);
        companies.add(company3);

        assertThatExceptionOfType(DuplicatedElementException.class).isThrownBy(() -> {
            service.findCompanyById(companies, 1);
        });
    }

    @Test
    void hasSufficiantBalance() {
        Company company = new Company();
        company.setBalance(10);

        assertThat(service.hasSufficiantBalance(company, 10)).isTrue();

        assertThatExceptionOfType(CompanyBalanceInsufficientException.class).isThrownBy(() -> {
            service.hasSufficiantBalance(company, 11);
        });
    }
}