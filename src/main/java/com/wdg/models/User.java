package com.wdg.models;

import lombok.Data;

import java.util.List;

@Data
public class User {
    private long id;
    private List<Balance> balance;
}
