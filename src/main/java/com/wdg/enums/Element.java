package com.wdg.enums;

public enum Element {
    COMPANY("Company"),
    USER("User");

    public final String label;

    Element(String label) {
        this.label = label;
    }
}
