package com.wdg.models;

import com.wdg.enums.WalletType;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DistributionRequest {
    private WalletType walletType;
    private long companyId;
    private long userId;
    private int amount;
}
