package com.wdg.models;

import lombok.Data;

import java.util.List;

@Data
public class JsonData {
    private List<Wallet> wallets;
    private List<Company> companies;
    private List<User> users;
    private List<Distribution> distributions;
}
