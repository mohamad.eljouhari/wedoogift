package com.wdg.models;

import com.wdg.enums.WalletType;
import lombok.Data;

@Data
public class Wallet {
    private long id;
    private String name;
    private WalletType type;
}
