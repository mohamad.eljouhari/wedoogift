package com.wdg.config;

import com.wdg.services.ICompanyService;
import com.wdg.services.IUserService;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

@Profile("test")
@Configuration
public class GiftsServiceTestConfig {

    @Bean
    @Primary
    public ICompanyService companyService() {
        return Mockito.mock(ICompanyService.class);
    }

    @Bean
    @Primary
    public IUserService userService() {
        return Mockito.mock(IUserService.class);
    }
}
