package com.wdg.enums;

public enum WalletType {

    GIFT("Gift"),
    FOOD("Food");

    public final String label;

    WalletType(String label) {
        this.label = label;
    }
}
