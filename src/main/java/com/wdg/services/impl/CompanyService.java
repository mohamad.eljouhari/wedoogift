package com.wdg.services.impl;

import com.wdg.enums.Element;
import com.wdg.exceptions.custom.CompanyBalanceInsufficientException;
import com.wdg.exceptions.custom.CompanyNotFoundException;
import com.wdg.exceptions.custom.DuplicatedElementException;
import com.wdg.models.Company;
import com.wdg.services.ICompanyService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CompanyService implements ICompanyService {

    @Override
    public com.wdg.models.Company findCompanyById(List<com.wdg.models.Company> companies, long id) throws CompanyNotFoundException, DuplicatedElementException {
        List<com.wdg.models.Company> matches = companies.stream().filter(u -> u.getId() == id).collect(Collectors.toList());
        if (matches.isEmpty())
            throw new CompanyNotFoundException(String.valueOf(id));
        else if (matches.size() > 1)
            throw new DuplicatedElementException(Element.COMPANY.label);
        else
            return matches.get(0);
    }

    @Override
    public boolean hasSufficiantBalance(Company company, int amount) throws CompanyBalanceInsufficientException {
        if (company.getBalance() < amount)
            throw new CompanyBalanceInsufficientException();
        return true;
    }

}
