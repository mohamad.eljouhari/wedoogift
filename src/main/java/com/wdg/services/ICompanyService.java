package com.wdg.services;

import com.wdg.exceptions.custom.CompanyBalanceInsufficientException;
import com.wdg.exceptions.custom.CompanyNotFoundException;
import com.wdg.exceptions.custom.DuplicatedElementException;
import com.wdg.models.Company;

import java.util.List;

public interface ICompanyService {

    Company findCompanyById(List<Company> companies, long id) throws CompanyNotFoundException, DuplicatedElementException;

    boolean hasSufficiantBalance(Company company, int amount) throws CompanyBalanceInsufficientException;

}
