package com.wdg.services.impl;

import com.wdg.enums.WalletType;
import com.wdg.exceptions.custom.CompanyNotFoundException;
import com.wdg.exceptions.custom.DuplicatedElementException;
import com.wdg.exceptions.custom.UserNotFoundException;
import com.wdg.helpers.FileHelper;
import com.wdg.models.*;
import com.wdg.services.ICompanyService;
import com.wdg.services.IGiftsService;
import com.wdg.services.IUserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@Slf4j
@Service
@RequiredArgsConstructor
public class GiftsService implements IGiftsService {

    private final ICompanyService companyService;
    private final IUserService userService;
    private final ReentrantReadWriteLock rwLock = new ReentrantReadWriteLock();
    private final Lock readLock = rwLock.readLock();
    private final Lock writeLock = rwLock.writeLock();
    @Value("${jsonData.inputfile}")
    private String inputFilePath;
    @Value("${jsonData.outputfile}")
    private String outputFilePath;
    private JsonData data;
    private Map<WalletType, Long> wallets = new HashMap<>();

    @PostConstruct
    private void initData() throws FileNotFoundException {
        if (Objects.isNull(data)) {
            loadData();
        }
    }

    @Override
    public JsonData distribute(WalletType distributionType, long companyId, long userId, int amount) throws CompanyNotFoundException,
            UserNotFoundException, DuplicatedElementException {

        Company company;
        User user;

        readLock.lock();
        try {
            company = companyService.findCompanyById(data.getCompanies(), companyId);
            user = userService.findUserById(data.getUsers(), userId);
        } finally {
            readLock.unlock();
        }

        if (Objects.nonNull(company) && companyService.hasSufficiantBalance(company, amount) && Objects.nonNull(user)) {
            writeLock.lock();
            try {
                data.getDistributions().add(newDistribution(distributionType, company, user, amount));
            } finally {
                writeLock.unlock();
            }
        }
        FileHelper.updateOutput(outputFilePath, data);

        return data;
    }

    @Override
    public List<Balance> calculateUserBalance(long userId) throws UserNotFoundException, DuplicatedElementException {
        List<Distribution> distributions;
        User user;

        readLock.lock();
        try {
            user = userService.findUserById(data.getUsers(), userId);
            distributions = userService.getUserDistributions(data.getDistributions(), user.getId());
        } finally {
            readLock.unlock();
        }

        List<Balance> balances = new ArrayList<>();
        data.getWallets().forEach(w -> balances.add(userService.calculateBalance(distributions, w)));

        writeLock.lock();
        try {
            user.setBalance(balances);
        } finally {
            writeLock.unlock();
        }
        FileHelper.updateOutput(outputFilePath, data);
        return balances;
    }


    /*********************************************/


    private void loadData() throws FileNotFoundException {
        try {
            data = FileHelper.readFile(outputFilePath);
        } catch (FileNotFoundException o) {
            log.warn("No output file \"first endowment\" loading input file ...\n");
            data = FileHelper.readFile(inputFilePath);
        }
        data.getWallets().forEach(w -> wallets.put(w.getType(), w.getId()));
    }

    private Distribution newDistribution(WalletType distributionType, Company company, User user, int amount) {
        Distribution distribution = new Distribution()
                .id(data.getDistributions().size() + 1)
                .wallet_id(wallets.get(distributionType))
                .amount(amount)
                .start_date(LocalDate.now())
                .company_id(company.getId())
                .user_id(user.getId());
        if (distributionType.equals(WalletType.GIFT))
            distribution.end_date(LocalDate.now().plusDays(365));
        else if (distributionType.equals(WalletType.FOOD))
            distribution.end_date(calculateFoodEndDate());

        company.setBalance(company.getBalance() - amount);
        userService.updateBalance(user, wallets.get(distributionType), amount);
        return distribution;
    }

    private LocalDate calculateFoodEndDate() {
        int year = LocalDate.now().getYear() + 1;
        int day = LocalDate.of(year, 2, 1).lengthOfMonth();
        return LocalDate.of(year, 2, day);
    }

    public JsonData getData() {
        return data;
    }
}
